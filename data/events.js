var events_data = {
    "events": {
        "algoholics": {
            "title": "Algoholics",
            "quote": "“An algorithm must be seen to be believed.” - Donald Ervin Knuth",
            "details": "<h3>### Team Size: 2</h3><h3>### Number of Rounds: 2</h3> <br/><p><strong>Round 1 (Prelims)</strong></p><hr/> <p>Multiple-choice based questions testing the participant’s algorithmic competency.</p> <br/><p><strong>Round 2 (Mains)</p><hr/> <p>The qualifying teams would be given a set of problems to develop an algorithm/pseudo-code on, followed by a presentation.</p>"
        },
        "brainspark": {
            "title": "BrainSpark",
            "quote": "“You can make anything happen if you put your mind to it.” - Lori Greiner",
            "details": "<h3>### Team Size: 2</h3><h3>### Number of Rounds: 2</h3><br/><p><strong>Round 1 (Prelims)</strong></p><hr/><p>Multiple Choice Questions testing the logical and quantitative ability of participants.</p><br/><p><strong>Round 2 (Mains)</strong></p><hr/><p>The qualifying teams would be given a set of puzzles to solve.</p>"
        },
        "brainspark2": {
            "title": "BrainSpark",
            "quote": "“You can make anything happen if you put your mind to it.” - Lori Greiner",
            "details": "<h3>### Team Size: 2</h3><h3>### Number of Rounds: 2</h3><br/><p><strong>Round 1 (Prelims)</strong></p><hr/><p>Multiple Choice Questions testing the logical and quantitative ability of participants.</p><br/><p><strong>Round 2 (Mains)</strong></p><hr/><p>The qualifying teams would be given a set of puzzles to solve.</p>"
        },
        "brainspark3": {
            "title": "BrainSpark",
            "quote": "“You can make anything happen if you put your mind to it.” - Lori Greiner",
            "details": "<h3>### Team Size: 2</h3><h3>### Number of Rounds: 2</h3><br/><p><strong>Round 1 (Prelims)</strong></p><hr/><p>Multiple Choice Questions testing the logical and quantitative ability of participants.</p><br/><p><strong>Round 2 (Mains)</strong></p><hr/><p>The qualifying teams would be given a set of puzzles to solve.</p>"
        },
        "brainspark4": {
            "title": "BrainSpark",
            "quote": "“You can make anything happen if you put your mind to it.” - Lori Greiner",
            "details": "<h3>### Team Size: 2</h3><h3>### Number of Rounds: 2</h3><br/><p><strong>Round 1 (Prelims)</strong></p><hr/><p>Multiple Choice Questions testing the logical and quantitative ability of participants.</p><br/><p><strong>Round 2 (Mains)</strong></p><hr/><p>The qualifying teams would be given a set of puzzles to solve.</p>"
        },
        "code-a-thon": {
            "title": "Code-a-thon",
            "quote": "Do you have what it takes to survive a coding marathon? Then we have just the right thing for you..",
            "details": "<h3>### Team Size: 2</h3><h3>### Operating System: Windows</h3><br/><p><strong>Platform: Dev C++</strong></p>Round 1 (Prelims)</strong></p><hr/><p> Multiple-choice questions based on concepts of C and C++.</p><br/><p><strong>Round 2</strong></p><hr/><p> The qualifying teams would be given a set of programming problems to code in C/C++.</p><br/><p><strong>Round 3 (mains)</strong></p><hr/><p> The qualifying teams would be given a multi-modular project to be developed in a span of 5 hours.</p>"
        },
        "debug++": {
            "title": "Debug++",
            "quote": "“Programming allows you to think about thinking, and while debugging you learn learning.” - Nicholas Negroponte",
            "details": "<p>Only when you are debugging somebody else’ s program you understand that only 10 % of the programmers are above average...are you among them?</p><br/><h3>### Team Size : 2</h3><h3>### Number of Rounds: 2</h3><br/><p> <strong>Operating System:</strong> Windows <br/> <strong>Platform:</strong> Dev - C++</p><br/><p><strong>Round 1(Prelims)</strong></p><p>Multiple - choice questions based on standard C / C++concepts.</p><br/><p><strong>Round 2(Mains)</strong></p><p>The qualifying teams would be given a set of certain C / C++codes to debug.</p>"
        },
        "java-juggling": {
            "title": "Java Juggling",
            "details": "<h3>### Team Size: 2</h3><br/><p><strong>Operating System: </strong>Linux<br/><strong>Platform: </strong>NetBeans</p><br/><p><strong>Round 1 (Prelims)</strong></p><hr/><p>Multiple-choice questions based on concepts of Java</p><br/><p><strong>Round 2 (Mains)</strong></p><hr/><p>The qualifying teams would be given a set of programming problems to code in Java.</p>"
        },
        "jam": {
            "title": "JUST a Minute (JAM)",
            "quote": "Every single second matters, every single round matters. You’ve got to fight every second, every minute and every round.",
            "details": "<h3>### Team Size: 2</h3><br/><p>Games would have to be completed in a minute period.</p><p>A maximum of 72 teams would be allowed.</p>"
        },
        "mind-matters": {
            "title": "Mind Matters",
            "quote": "Think you’re Mr. Know-it-All of the silicon world? Come and participate in this event, test your knowledge, let the world know that and walk away as a champion.",
            "details": "<h3>### Team Size: 2</h3><h3>### No of Rounds: 2</h3><br/><p><strong>Round 1 (Prelims)</strong></p><hr/><p>Multiple-choice questions based on Computer Science and IT world.</p><br/><p><strong>Round 2 (Mains)</strong></p><hr/><p>The qualifying teams would compete in the Event's Main Quiz.</p>"
        },
        "select-*-from-brain": {
            "title": "Select * from Brain",
            "quote": "Select , Ignite , Fire…Prove that you have the answer to all queries.!!!",
            "details": "<h3>### Team Size: 2</h3><br/><p><strong>Operating System:</strong> Linux<br/><strong>Platform:</strong> MySQL 5.5</p><br/><p><strong>Round 1 (Prelims)</strong></p><hr/><p>Multiple-choice questions based on standard SQL, XQuery and DBMS concepts.</p><br/><p><strong>Round 2 (Mains)</strong></p><hr/><p>The teams qualifying would be given a set of queries, that would have to be solved using SQL.</p>"
        },
        "spin-a-web": {
            "title": "Spin a Web",
            "quote": "Have you been bitten by the creative bug? Then we have just the right antidote. Spin on the creativity loom a web-site of Imagination,Innovation and Design.",
            "details": "<h3>### Team Size: 2</h3><h3>### No of Rounds: 2</h3><br/><p><strong>Operating System:</strong> Linux<br/><strong>Platform:</strong> MySQL 5.5</p><br/><p><strong>Round 1 (Prelims)</strong></p><hr/><p>Multiple-choice questions based on internet, Web Servers, Web Development, HTML, CSS &amp; Javascript and PHP.</p><p><strong>Round 2 (Mains)</strong></p><hr/><p>The qualifying teams would be asked to develop a website using HTML, CSS, PHP and MySQL, in a span of 4 hours.</p>"
        },
        "tunrcoat": {
            "title": "Turncoat",
            "quote": "Get ready to drive through a battle of thoughts with your own self on a buzzer press.",
            "details": "<h3>### Team Size: 1</h3><h3>### No of Rounds: 2</h3><br/><p><strong>Round 1</strong></p><hr/><p>Each participant will be given 2 minutes to speak on a debatable topic, with alternative durations of 30 seconds each in favor or against the issue.</p><br/><p><strong>Round 2</strong></p><hr/><p>Two participants will be given the same topic to speak. Alternatively, they will have to speak in favor and against.</p><br/><p><strong>Round 3</strong></p><hr/><p>All qualifying participants will be given the same topic to speak. They would randomly be asked to present their views in contradiction to the previous speaker.</p>"
        }
    }
}