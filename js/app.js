var closeBtn = "<?xml version='1.0' encoding='iso-8859-1'?><svg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 286.054 286.054' style='enable-background:new 0 0 286.054 286.054;' xml:space='preserve'><g><path style='fill:#E2574C;' d='M168.352,142.924l25.28-25.28c3.495-3.504,3.495-9.154,0-12.64l-12.64-12.649c-3.495-3.486-9.145-3.495-12.64,0l-25.289,25.289l-25.271-25.271c-3.504-3.504-9.163-3.504-12.658-0.018l-12.64,12.649c-3.495,3.486-3.486,9.154,0.018,12.649l25.271,25.271L92.556,168.15c-3.495,3.495-3.495,9.145,0,12.64l12.64,12.649c3.495,3.486,9.145,3.495,12.64,0l25.226-25.226l25.405,25.414c3.504,3.504,9.163,3.504,12.658,0.009l12.64-12.64c3.495-3.495,3.486-9.154-0.009-12.658L168.352,142.924z M143.027,0.004C64.031,0.004,0,64.036,0,143.022c0,78.996,64.031,143.027,143.027,143.027s143.027-64.031,143.027-143.027C286.054,64.045,222.022,0.004,143.027,0.004z M143.027,259.232c-64.183,0-116.209-52.026-116.209-116.209s52.026-116.21,116.209-116.21s116.209,52.026,116.209,116.209S207.21,259.232,143.027,259.232z'/></g></svg>";

function activeNone() {
    var nav_btns = document.querySelectorAll(".nav_item");
    for (var i = nav_btns.length - 1; i >= 0; i--) {
        nav_btns[i].classList.remove("active");
    }
}

var Background = function() {
    var canvas_el = document.createElement("canvas");
    canvas_el.id = "canvas";
    document.body.insertBefore(canvas_el, document.body.firstChild);
    var canvas = document.getElementById('canvas'),
        ctx = canvas.getContext('2d'),
        colorDot = '#007bff',
        color = '#007bff';
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    canvas.style.display = 'block';
    ctx.fillStyle = colorDot;
    ctx.lineWidth = .1;
    ctx.strokeStyle = color;

    var mousePosition = {
        x: 30 * canvas.width / 100,
        y: 30 * canvas.height / 100
    };

    var dots = {
        nb: (window.innerWidth > 700) ? 300 : 150,
        distance: (window.innerWidth > 700) ? 160 : 100,
        d_radius: (window.innerWidth > 700) ? 100 : 60,
        array: []
    };

    function Dot() {
        this.x = Math.random() * canvas.width;
        this.y = Math.random() * canvas.height;

        this.vx = -.5 + Math.random();
        this.vy = -.5 + Math.random();

        this.radius = Math.random();
    }

    Dot.prototype = {
        create: function() {
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
            ctx.fill();
        },

        animate: function() {
            for (i = 0; i < dots.nb; i++) {

                var dot = dots.array[i];

                if (dot.y < 0 || dot.y > canvas.height) {
                    dot.vx = dot.vx;
                    dot.vy = -dot.vy;
                } else if (dot.x < 0 || dot.x > canvas.width) {
                    dot.vx = -dot.vx;
                    dot.vy = dot.vy;
                }
                dot.x += dot.vx;
                dot.y += dot.vy;
            }
        },

        line: function() {
            for (i = 0; i < dots.nb; i++) {
                for (j = 0; j < dots.nb; j++) {
                    i_dot = dots.array[i];
                    j_dot = dots.array[j];

                    if ((i_dot.x - j_dot.x) < dots.distance && (i_dot.y - j_dot.y) < dots.distance && (i_dot.x - j_dot.x) > -dots.distance && (i_dot.y - j_dot.y) > -dots.distance) {
                        if ((i_dot.x - mousePosition.x) < dots.d_radius && (i_dot.y - mousePosition.y) < dots.d_radius && (i_dot.x - mousePosition.x) > -dots.d_radius && (i_dot.y - mousePosition.y) > -dots.d_radius) {
                            ctx.beginPath();
                            ctx.moveTo(i_dot.x, i_dot.y);
                            ctx.lineTo(j_dot.x, j_dot.y);
                            ctx.stroke();
                            ctx.closePath();
                        }
                    }
                }
            }
        }
    };

    function createDots() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        for (i = 0; i < dots.nb; i++) {
            dots.array.push(new Dot());
            dot = dots.array[i];

            dot.create();
        }

        dot.line();
        dot.animate();
    }

    window.onmousemove = function(parameter) {
        mousePosition.x = parameter.pageX;
        mousePosition.y = parameter.pageY;
    }


    window.onresize = function() {
        document.body.removeChild(canvas);
        Background();
        return false;
    }

    mousePosition.x = window.innerWidth / 2;
    mousePosition.y = window.innerHeight / 2;

    setInterval(createDots, 1000 / 30);
};

var Navigation = function() {
    var bg = new Background();
    this.footer_content = '<span>Copyright 2017 DUCS</span><span>Designed by Sudhanshu Vishnoi</span>'
    this.init = function() {
        var main = document.getElementById("main");
        main.innerHTML = '<h1 class="title-home" data-text="Sankalan 17"><img src="images/logo.png"/></h1>';
        var ht = "";
        if (window.innerWidth <= 700) ht += '<div id="nav-icon"><?xml version="1.0" encoding="iso-8859-1"?><!DOCTYPE svg  PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg fill="#fff" height="32px" id="Layer_1" style="enable-background:new 0 0 32 32;" version="1.1" viewBox="0 0 32 32" width="32px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M4,10h24c1.104,0,2-0.896,2-2s-0.896-2-2-2H4C2.896,6,2,6.896,2,8S2.896,10,4,10z M28,14H4c-1.104,0-2,0.896-2,2  s0.896,2,2,2h24c1.104,0,2-0.896,2-2S29.104,14,28,14z M28,22H4c-1.104,0-2,0.896-2,2s0.896,2,2,2h24c1.104,0,2-0.896,2-2  S29.104,22,28,22z"/></svg></div>'
        ht += "<div class='nav_item' id='about'>About</div>";
        ht += "<div class='nav_item' id='events'>Events</div>";
        ht += "<div class='nav_item' id='team'>Team</div>";
        ht += "<div class='nav_item' id='sponsors'>Sponsors</div>";
        ht += "<div class='nav_item' id='srijan'>Srijan</div>";
        ht += "<div class='nav_item' id='contact'>Contact</div>";
        ht += "<div class='nav_item' id='register'>Register</div>";
        // ht += "<div class='nav_item' id='accomodation'>Accomodation</div>";
        ht += '<div class="social_btn"><a target="_blank" href="https://www.facebook.com/DUCS.Sankalan/" title="Like our FaceBook Page"><?xml version="1.0" encoding="iso-8859-1"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="60.734px" height="60.733px" viewBox="0 0 60.734 60.733" style="enable-background:new 0 0 60.734 60.733;" xml:space="preserve"><g><path fill="#3b5998" d="M57.378,0.001H3.352C1.502,0.001,0,1.5,0,3.353v54.026c0,1.853,1.502,3.354,3.352,3.354h29.086V37.214h-7.914v-9.167h7.914v-6.76c0-7.843,4.789-12.116,11.787-12.116c3.355,0,6.232,0.251,7.071,0.36v8.198l-4.854,0.002c-3.805,0-4.539,1.809-4.539,4.462v5.851h9.078l-1.187,9.166h-7.892v23.52h15.475c1.852,0,3.355-1.503,3.355-3.351V3.351C60.731,1.5,59.23,0.001,57.378,0.001z"/></g></svg></a></div>'

        document.getElementById("nav").innerHTML = ht;

        if (window.innerWidth <= 700) document.getElementById("nav-icon").onclick = function() {
            document.getElementById("nav").classList.toggle("active")
        }

        var footer = this.footer_content;
        document.getElementById("footer").innerHTML = footer;

        if (nav.footer_content.indexOf(atob("RGVzaWduZWQgYnkgU3VkaGFuc2h1IFZpc2hub2k=")) < 0) console.log(atob("Q3JlZGl0cyBmb3IgZGVzaWduIFN0b2xlbiEh"))

        var nav_btns = document.querySelectorAll(".nav_item");
        for (var i = nav_btns.length - 1; i >= 0; i--) {
            nav_btns[i].addEventListener("click", handleRequest)
        }

        function handleRequest(ev) {
            var nav_btns = document.querySelectorAll(".nav_item");
            for (var i = nav_btns.length - 1; i >= 0; i--) {
                nav_btns[i].classList.remove("active");
            }
            ev.srcElement.classList.add("active");
            document.getElementById("nav").classList.remove("active");
            switch (ev.srcElement.id) {
                case "about":
                    About();
                    break;
                case "events":
                    Events();
                    break;
                case "team":
                    Team();
                    break;
                case "home":
                default:
                    Home();
            }
        }

    }
}

function Home() {
    console.log("show home")
    activeNone();
    history.replaceState(null, null, "/#/home/")
    var ht = '<h1 class="title-home"><img src="images/logo.png"/></h1> <div id="sponsors_home">';
    for (var i = sponsors.length - 1; i >= 0; i--) {
        ht += '<img title="' + sponsors[i].name + '" src="' + sponsors[i].image + '"/>'
    }
    ht += "</div>"
    document.getElementById("main").innerHTML = ht

    if (document.getElementById("footer").innerHTML.indexOf(atob("RGVzaWduZWQgYnkgU3VkaGFuc2h1IFZpc2hub2k=")) < 0) console.log(atob("Q3JlZGl0cyBmb3IgZGVzaWduIFN0b2xlbiEh"))
}

function About() {
    var content = "<p>Sankalan was conceived in 2005 as an annual technical festival with the aim of encouraging young minds to engage with technology through a wide array of technical and non technical events that promise to educate and inspire.</p><p>Over the years, Sankalan has grown to be a widely anticipated event that sees wide participation from students of various prestigious institutions. Be it quizzing and coding or debating and gaming, Sankalan has something to offer for everyone.</p><p>So, we welcome you to Sankalan 2017 with open arms. Come celebrate tech with us.</p>"
    history.replaceState(null, null, "/#/about/")
    var main = document.getElementById("main");
    main.classList.add("active");
    var ht = "<div id='close'>" + closeBtn + "</div>"
    ht += '<div class="about">' + content + '</div>';
    main.innerHTML = ht;


    document.getElementById("close").onclick = function() {
        main.classList.remove("active")
        Home();
    };
}


function typeIt(text) {
    var counter = 0;
    var event_details = document.getElementById("event_details");
    event_details.classList.add("active");
    var typewriter_interval = setInterval(doTypewriter, 10);

    function doTypewriter() {
        event_details.innerHTML = text.substr(0, counter) + "&block;";
        counter++;
        if (counter >= text.length) {
            clearInterval(typewriter_interval);
        }
    }
    document.getElementById("close").onclick = function() {
        event_details.classList.remove("events")
        event_details.classList.remove("active");
        Events();
        clearInterval(typewriter_interval);
    }
}

function showEventDetails(id) {
    var ev = events_data.events[id];
    history.replaceState(null, null, window.location.hash + id + "/")
    var main = document.getElementById("main");
    main.classList.add("active");
    main.innerHTML = "<div id='event_details'></div><div id='close'>" + closeBtn + "</div>";
    var ht = "";
    ht += "<h2 class='event_title'>## " + ev.title + "</h2>"
    if (ev.quote) ht += "<blockquote>" + ev.quote + "</blockquote>"
    ht += ev.details;
    typeIt(ht);
}

function Events() {
    console.log("show events")
    history.replaceState(null, null, "/#/events/")
    var main = document.getElementById("main");
    main.classList.add("active");

    var ht = "<div id='event_details'></div><div id='close'>" + closeBtn + "</div>"
    ht += "<div id='events_all'>"
    for (var event in events_data.events) {
        ht += "<div class='event_title' data-id='" + event + "'>" + events_data.events[event]["title"] + "</div>"
    }
    ht += "</div>"
    main.innerHTML = ht;

    document.getElementById("close").onclick = function() {
        main.classList.remove("active")
        Home();
    };
    var events_title = document.querySelectorAll(".event_title");
    for (var i = events_title.length - 1; i >= 0; i--) {
        events_title[i].onclick = function() {
            showEventDetails(this.getAttribute("data-id"))
        }
    }
}

function Team() {
    console.log("show team")
    var data = JSON.parse(localStorage.getItem("team"));
    history.replaceState(null, null, "/#/team/")
    console.log(data)
}


var nav;

function init() {
    nav = new Navigation();
    nav.init();
    var hash;
    try {
        hash = window.location.hash.split("#/")[1].split("/")[0];
    } catch (e) {
        hash = "home";
    }
    console.log("hash = ", hash)
    if (hash !== "home") document.getElementById(hash).classList.add("active");
    switch (hash) {
        case "about":
            About();
            break;
        case "events":
            hash = window.location.hash.split("#/")[1].split("/")[1];
            new Events();
            if (hash) showEventDetails(hash)
            break;
        case "home":
        default:
            Home();
    }
};

init();
